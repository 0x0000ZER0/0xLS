SRC=main.c
OUT=ls.out

debug:
	gcc -g -Wall -Wextra -fsanitize=leak,address,undefined $(SRC) -o $(OUT) -DZ0_DEBUG

release:
	gcc -O2 $(SRC) -o $(OUT)
