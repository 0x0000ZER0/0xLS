#include <dirent.h> 
#include <stdio.h> 
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <termios.h>
#include <unistd.h>

#define KEY_ESC 	0x1B
#define KEY_W		0x73
#define KEY_S		0x77
#define KEY_ENTER	0x0A

#define GREEN   "\x1B[32m"
#define DEFAULT "\x1B[0m"

typedef DIR 		dir;
typedef struct dirent 	node;
typedef struct termios	termios;

enum {
	STATE_FREE,
	STATE_BUSY
};

int
main(void) {
	int hnd;
	hnd = fileno(stdin);

	termios old_attr;
	tcgetattr(hnd, &old_attr);

	termios new_attr;
	memcpy(&new_attr, &old_attr, sizeof (old_attr));
	new_attr.c_lflag &= ~(ECHO | ICANON);
	tcsetattr(hnd, TCSANOW, &new_attr);

	int key;
	int pos;
	int state;

	pos	= 0;
	state	= STATE_FREE;

	while (true) {
		system("clear");	
		printf("Press [ESC] to stop the program.\n"
		       "Press [W] to move upwards.\n"
		       "Press [S] to move downwards.\n"
		       "Press [ENTER] to show the content or enter into the folder.\n\n");

		dir *curr;
		curr = opendir(".");
		if (curr == NULL) {
			perror("could not open the directory.\n");
			tcsetattr(hnd, TCSANOW, &old_attr);
			exit(EXIT_FAILURE);	
		}

		int idx;
		idx = 0;	

		node *picked;
		node *item;
		while (true) {
			item = readdir(curr);
			if (item == NULL)
				break;

			if (strcmp(item->d_name, ".") == 0)
				continue;

			switch (item->d_type) {
			case DT_DIR:
				if (idx == pos) {
					printf(GREEN "%s/\n" DEFAULT, item->d_name);
					picked = item;
				} else {
					printf("%s/\n", item->d_name);
				}

				++idx;
				break;
			case DT_REG:
				if (idx == pos) {
					printf(GREEN "%s\n" DEFAULT, item->d_name);
					picked = item;
				} else {
					printf("%s\n", item->d_name);
				}

				++idx;
				break;
			default:
				break;
			}
		}


		if (state == STATE_FREE)
			key = fgetc(stdin);

		if (key == KEY_ESC)
			break;
		
		switch (key) {
		case KEY_S:
			if (pos > 0)
				--pos;	
			state = STATE_FREE;
			break;
		case KEY_W:
			if (pos < idx - 1)
				++pos;
			state = STATE_FREE;
			break;
		case KEY_ENTER:
			if (picked->d_type == DT_REG) {
				FILE *file;
				file = fopen(picked->d_name, "r");
				if (file == NULL) {
					perror("could not open the file\n");
					closedir(curr);
					tcsetattr(hnd, TCSANOW, &old_attr);
					exit(EXIT_FAILURE);
				}
				
				fseek(file, 0, SEEK_END);
				long size;
				size = ftell(file);		
				fseek(file, 0, SEEK_SET);
				
				char *content;
				content = malloc(sizeof (char) * size + 1);
				fread(content, sizeof (char), size, file);
				content[size * sizeof (char)] = '\0';
				
				printf("---------------- CONTENT ----------------\n");
				printf("%s\n", content);
				free(content);

				fclose(file);
				
				key = fgetc(stdin);
				state = STATE_BUSY;
			} else if (picked->d_type == DT_DIR) {
				chdir(picked->d_name);
			}	 
			break;
		default:
			break;
		}
		closedir(curr);
	}

	tcsetattr(hnd, TCSANOW, &old_attr);
	return 0;	
}
